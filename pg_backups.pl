#!perl
#
#
use Getopt::Long;
use DBI;
## uses DBD::PgPP as the PostgreSQL driver.
#
# you can override these defaults using the cli options.
my $dbaseName     = ""; #  database name.
my $dbaseUser     = ""; #  username for your backup account.
my $dbasePassword = ""; #  password for your backup account.
my $dbaseHost     = ""; #  host for the database.
my $dbasePort     = ""; #  port for the database.
my $VERBOSE       = 0;
my $DRYRUN        = 0;
#
my $PGDUMP        = "/usr/bin/pg_dump"; # change this to be the fully qualified path to pg_dump.
#
# DO NOT CHNAGE ANYTHING BELOW THIS LINE.
#
my $VERSION       = "0.8.5";
#
GetOptions( "databaseName|dbaseName|dbname=s"         => \$dbaseName,
            "databaseUsername|dbaseUser|dbuser=s"     => \$dbaseUser,
            "databasePassword|dbasePassword|dbpass=s" => \$dbasePassword,
            "pgdump|pg_dump=s"                        => \$PGDUMP,
            "databaseHost|dbaseHost|dbhost=s"         => \$dbaseHost,
            "databasePort|dbasePort|dbport=s"         => \$dbasePort,
            "verbose|VERBOSE|v"                       => \$VERBOSE,
            "dryrun|dry-run|dr"                       => \$DRYRUN
          );
#
#
#
if ( length( $dbaseName ) < 2 ) 
      {
      &displayHelp; 
      exit 1; 
      }
$dbaseHost = ( length( $dbaseHost) > 0 ? $dbaseHost : "127.0.0.1" );
$dbasePort = ( length( $dbasePort) > 0 ? $dbasePort : "5432" );
#
sub currentDate
      {
      my ($minute,$hour,$day,$month,$year) = ( localtime( time ) )[ 1,2,3,4,5 ];
      $month += 1;
      $year  += 1900;
      return sprintf "%04d-%02d-%02d__%02d-%02d", $year,$month,$day,$hour,$minute;
      }
#
sub displayHelp
      {
      print "$0 - HELP\n\n";
      print "  this script backs up all the table of the given database.\n\n";
      print "  options:\n";
      print "    -databaseName \"<databaseName>\"\n";
      print "    -databaseUsername \"<username>\" [optional]\n";
      print "    -databasePassword \"<password>\" [optional]\n";
      print "    -databaseHost \"<hostname>\" [optional]\n";
      print "    -databasePort \"<port>\" [optional]\n";
      print "    -pgdump \"<path to pg_dump>\" [optional]\n";
      print "    -verbose [optional]\n";
      print "    -dryrun [optional]\n";
      print "\n  version - $VERSION\n\n";
      }
#
# MAIN
#
if ( $VERBOSE || $DRYRUN ) { print "starting up.\n"; }
my $DBH = DBI->connect("dbi:PgPP:dbname=$dbaseName;host=$dbaseHost;port=$dbasePort", $dbaseUser, $dbasePassword , {AutoCommit => 0, RaiseError => 1});
#
if ( $VERBOSE ) { print DBD::PgPP::pgpp_server_identification( $DBH ) . "\n\n"; }
#
#
if ( $VERBOSE || $DRYRUN ) { print "getting table name(s).\n"; }
$STH = $DBH->prepare("select tablename as table from pg_tables where schemaname = 'public'");
$STH->execute();
while (my $row = $STH->fetchrow_hashref ) 
      {
      my $tableName = $row->{table};
      my $outputFile = sprintf "%s__%s.backup", currentDate, $tableName;
      my $CMD = sprintf "%s -t %s -d %s -h %s -p %s > %s", $PGDUMP, $tableName, $dbaseName, $dbaseHost, $dbasePort, $outputFile;
      if ( $VERBOSE || $DRYRUN ) { print "$CMD\n"; }
      if ( ! $DRYRUN ) 
            {
            open( PFH, "$CMD |" ) or die "cant open process handle.";
            foreach my $line ( <PFH> ) { print $line; }
            close PFH;            
            }
      }
#
