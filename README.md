# README #

This a perl (perl5) script that backs up all tables in a given database.  This makes it nice and easy to backup your database, especially when the the database structure changes (add/remove tables) and the backup script picks up the changes automagically.

### How do I get set up? ###

* clone the repo (or download the script)
* make sure the necessary perl modules are installed (we intentually made the list small and simple to install)
* put the script into cron with the appropriate parameters

### documentation ###

there is no documentation at this point.

here is an example of cron entry.

```
30 3 * * * /home/backups/bin/pg_backups.pl -databaseName "productionApp" 
```

this run the script at 3:30 am every day.

### Who do I talk to? ###

* contact support at nocturnalcodingmonkeys.com if you have any issues.